#!/bin/sh
echo "Enter project name"
read project
mkdir $project
cd $project
mkdir src
echo "Enter virtual-environment name"
read venv
echo "Activating "$venv" virtual environment"
python3 -m venv $venv
source venv/bin/activate
echo "installing django"
pip install django
cd src
echo "creating base-directory "$project
django-admin startproject $project
cd $project
echo"Enter app-name(compulsary)"
read appname
django-admin startapp $appname 
echo "app created succesfully"
echo "lauching services at localhost:8000"
python manage.py runserver





